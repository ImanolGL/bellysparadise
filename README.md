# bellysparadise

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
##EXPLICACIÓN PROYECTO
Aplicación de lectura de API, sobre alcohol.

\*INTEGRANTES
Anto, Unai, Cesar y Joseba

\*ESTANDAR CÓDIGO
Uso de formateador Prettier, con directiva de archivo .prettierrc

\*ACUERDOS
Por votación, o por carta mas alta

\*REPO GIT
https://gitlab.com/cesarogarcia/los-piripis-1.0.git

\*COMANDOS GIT
Para poder fusionar todos los cambios que se han hecho en el repositorio local trabajando, el comando que se usa es:

\*PARA SUBIR A EL REPO

git pull (para bajar)
git status (verde o rojo)
git add . (para añadir contenido)
git commit (local)
git status (verde o rojo)
git push (para subir)

\*PARA BAJAR DEL REPO

git pull
git status
Ver si hay conflictos

\*Comenzar un repo

    git init   → inicializar el proyecto local
    git remote add ...   → linka con el repo
    git clone <url> → crea una copia en local del repositorio

\*Cotidianos
		****IMPORTANTE****
    
    git pull    → Descargar del repo
    git add <filenames>   → Añade al commit el archivo (o archivos)
    git add . → Añade al commit todos los cambios
    git commit -m "mensaje del commit"   → commit. Esto en local
    git status → Muestra el estado en local del repositorio
    git push  → subir al repo

\*Para que no nos pida la contraseña todas las veces

Ver consideraciones de seguridad en https://git-scm.com/book/es/v2/Herramientas-de-Git-Almacenamiento-de-credenciales

    git config --global credential.helper cache → Guarda la contraseña durante un tiempo
    git config --global credential.helper store → Guarda la contraseña permanentemente en ~/.git-credentials
