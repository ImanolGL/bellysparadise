import { shallowMount } from "@vue/test-utils";
import StartScreen from "@/components/StartScreen.vue";

test("emite evento click", async () => {
  const task = {
    completed: true
  };
  const wrapper = shallowMount(StartScreen, {
    propsData: {
      task: task
    }
  });
  expect(wrapper.emitted().click).toBe(undefined);
  const button = wrapper.find(".button_cat");
  button.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.emitted().click.length.toBe(1));
  expect(wrapper.emitted().click[0]).toEqual([task]);
});
