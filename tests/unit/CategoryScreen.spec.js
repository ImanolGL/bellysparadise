import { shallowMount, mount } from "@vue/test-utils";
import CategoryScreen from "@/components/CategoryScreen.vue";

test("Comprobamos que la información del padre (Lista categoria) imprimimos ok en pantalla", () => {
  const wrapper = mount(CategoryScreen);
  const categorias = wrapper.findAll(".category").wrappers;

  expect(categorias[0].text()).toBe("Vegan");
  expect(categorias[1].text()).toBe("Seafood");
  expect(categorias[7].text()).toBe("Beef");
});
test("comprobamos que emitimos evento cuando nos llega un click y enviamos info a nuestro padre con el nombre de categoria", async () => {
  const wrapper = mount(CategoryScreen);

  expect(wrapper.emitted().click).toBe(undefined);

  const button = wrapper.findAll(".category").wrappers;
  button[0].vm.$emit("categoryClickFirst", "Vaca");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted().categoryClick.length).toBe(1);
  expect(wrapper.emitted().categoryClick[0]).toEqual(["Vaca"]);
});
console.log("category");
