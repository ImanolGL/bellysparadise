import { shallowMount } from "@vue/test-utils";
import trendly_category_button from "@/components/trendly_category_button";

test("Comprobamos que la información del padre (LIsta categoria) imprimimos ok en pantalla", () => {
  const wrapper = shallowMount(trendly_category_button, {
    propsData: {
      categoryName: "trendly"
    }
  });
  expect(wrapper.text()).toContain("trendly");
});

test("comprobamos que emitimos evento cuando nos llega un click y enviamos info a nuestro padre con el nombre de categoria", async () => {
  const wrapper = shallowMount(trendly_category_button, {
    propsData: { categoryName: "trendly" }
  });

  expect(wrapper.emitted().click).toBe(undefined);

  const button = wrapper.find("button");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted().categoryClickFirst.length).toBe(1);
  expect(wrapper.emitted().categoryClickFirst[0]).toEqual(["trendly"]);
});
/*Boton categoria2*/
test("Comprobamos que la información del padre (Lista categoria) imprimimos ok en pantalla", () => {
  const wrapper = shallowMount(trendly_category_button, {
    propsData: {
      categoryName: "trendly"
    }
  });
  expect(wrapper.text()).toContain("trendly");
});
