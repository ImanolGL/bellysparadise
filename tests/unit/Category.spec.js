import { shallowMount } from "@vue/test-utils";
import Category from "@/components/Category.vue";

test("Comprobamos que la información del padre (LIsta categoria) imprimimos ok en pantalla", () => {
  const wrapper = shallowMount(Category, {
    propsData: {
      categoryName: "Vaca"
    }
  });
  expect(wrapper.text()).toContain("Vaca");
});

test("comprobamos que emitimos evento cuando nos llega un click y enviamos info a nuestro padre con el nombre de categoria", async () => {
  const wrapper = shallowMount(Category, {
    propsData: { categoryName: "Vaca" }
  });

  expect(wrapper.emitted().click).toBe(undefined);

  const button = wrapper.find("button");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted().categoryClickFirst.length).toBe(1);
  expect(wrapper.emitted().categoryClickFirst[0]).toEqual(["Vaca"]);
});
/*Boton categoria2*/
test("Comprobamos que la información del padre (Lista categoria) imprimimos ok en pantalla", () => {
  const wrapper = shallowMount(Category, {
    propsData: {
      categoryName: "Vaca"
    }
  });
  expect(wrapper.text()).toContain("Vaca");
});
